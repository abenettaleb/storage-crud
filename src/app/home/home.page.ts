import { Component, ViewChild } from '@angular/core';
import {StorageService, Item} from '../services/storage.service';
import {Platform, ToastController, IonList} from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  items: Item[] = [];
  newItem: Item = {} as Item;
  @ViewChild('myList')myList: IonList;
  constructor(private storageService: StorageService, private plt: Platform, private toastController: ToastController) {
    this.plt.ready().then(() => {
      this.loadItems();
    });
  }

  // CREATE
  addItem() {
    this.newItem.modified = Date.now();
    this.newItem.id = Date.now();
    this.storageService.addItem(this.newItem).then(item => {
      this.newItem = ({} as Item);
      this.showToast('Item added!');
      this.loadItems(); // or add it to the array directly
    });
  }

  // READ
  loadItems() {
    this.storageService.getItems().then(items => {
      this.items = items;
    });
  }

  // UPDATE
  updateItem(item: Item) {
    item.title = `${this.newItem.title}`;
    item.value = `${this.newItem.value}`;
    item.modified = Date.now();
    // tslint:disable-next-line:no-shadowed-variable
    this.storageService.updateItem(item).then(item => {
      this.newItem = ({} as Item);
      this.showToast('Item updated');
      this.myList.closeSlidingItems(); // Fix or sliding is stuck afterwards
      this.loadItems(); // Or update it inside the array directly
    });
  }

  // UPDATE
  deleteItem(item: Item) {
    // tslint:disable-next-line:no-shadowed-variable
    this.storageService.deleteItem(item.id).then(item => {
      this.showToast('Item removed');
      this.myList.closeSlidingItems(); // Fix or sliding is stuck afterwards
      this.loadItems(); // Or update it inside the array directly
    });
  }

  // Helper
  async showToast(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    await toast.present();
  }
}
